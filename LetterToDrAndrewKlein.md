# Letter to Dr Andrew Klein

Michael Ellis
Digital Scientific UK Ltd
6 Green End
The Commercial Centre
Comberton
Cambridge
CB23 7DY
UK

30-MAR-2020

Dear Dr Andrew Klein,

I have recently read your paper "Portable ventilators" https://tinyurl.com/w9wcp88 and on finding your email I write to ask your opinion of my observations in respect of the situation for those with severe respiratory problems caused by Covid-19.

When we first heard details about Coronavirus, and in particular its transmissivity I opined that this virus would not be containable. Further, regarding its severity, I also concluded that our hospitals and mainstream healthcare channels would most likely be overwhelmed.

It also occurs to me that in the absence of a vaccine and or any pharmaceutical intervention for a disease whose main symptom is an acute respiratory problem that ventilation and or some for oxygen therapy was likely to be the first-line defence. Is that correct?

Although preparations are being made for large scale field hospitals such as the Nightingale, I remain concerned there will be adequate staffing available. I also worry that such centralised Covid-19 hot-spots with all the to-ing and fro-ing of patients, doctors, nurses, catering staff, cleaners as so on may indeed facilitate the spread of the virus.

My question is, could simple non-invasive ventilation deployed at home be an effective measure and result in a significant net gain in lives saved?

My thoughts are as follows:

- Ventilators have become increasingly sophisticated, expensive and complex to use. Whilst this may suit well hospital use during 'peacetime', and indeed keep the manufacturers in business with the continued production and release of latest models, the current pandemic calls for a more 'wartime' make-do-and-mend solution where huge numbers of cheap, simple devices could be deployed at home.

- Hand-in-hand with such ventilator provision would need to come suitable training for the layperson. The aim here would not create a generation of nurses or doctors, but a massive number of people able to do better than nothing,  aiming at a net gain of lives saved. To this end, we at least have a proven effective means of distance learning provided through the internet.  I'm think of a short video tutorials in basic non-invasive ventilation technique.

- Home visits by professional health care workers, especially Covid-19 recoveries could perform home visits to assist.

- Distribution could be via the existing GP Surgery/Pharmacy network

I have no medical training. My field is medical imaging software within the field of cytogenetics. As such, I'm unqualified, so I seek to ask someone who will know better than I whether this could be a sensible strategy.

Some other benefits may follow:

"Empowering the People" and "Democratisation of Healthcare Provision" are catchphrases that could have a  psychological benefit to the public at large through their direct involvement.
Reduced loading on hospitals.
People are generally happier to live, and die, at home.

I have further doubts about how the government is handling the Covid-19 situation, but this one question, if it is not a stupid question seems at least worth asking.

Actually, I have always maintained that once we start castigating or deriding "stupid questions", we condemn areas of inquiry to convention rather than wisdom. Stupid questions are the way forward.

Sincerely yours

Michael Ellis

P.S. My company's small contribution to help in these difficult times is to make our karyotyping software entirely free to use for anyone wishing to perform karyotyping at home. We do have people from the Sanger Centre doing exactly this. https://tinyurl.com/wuw23yq  Please contact me if this is of any use to either yourself or anyone you may know. There is no charge and no commitment.







