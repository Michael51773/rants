# The Quantum Theory of Brexit and the eubit

Brexit is a quantum computing thing. Like quantum computing's qubit that can simultaneously exist in the 1 AND 0 state, the Brexit eubit can simultaneously exist in the of the  Leave and Remain states (and everything in between). 

This translates to a principle called superposition that claims that while we do not know what the state of Brexit is, it is actually in all possible states simultaneously, as long as we don't look to check.

Just as quantum calculations are sensitive to the slightest disturbance in a quantum system (say a stray photon) which causes the quantum computation to collapse, Brexit negotiations too are sensitive to the slightest disturbance in the political system, and hence collapse all the time.

Another feature in common is that of 'entanglement'. Knowing the political state of one entangled politician - left or right, allows one to know that the direction its mate is in the opposite direction, even when these politicians are separated by incredible distances (Einstein called it "spooky political action at a distance")

Taken together, political superposition and entanglement create an enormously enhanced dithering power that grows exponentially with the number of politicians. This is thought to be why we need another extension.

