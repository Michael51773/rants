<head>
  <meta charset="UTF-8">
  <meta name="10MillionFlies" content="Brexit Discussion">
  <meta name="keywords" content="EU,Brexit">
  <meta name="author" content="Michael Ellis">

<style>
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
}

blockquote {
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 1.5em 100px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
}
blockquote:before {
  color: #ccc;
  content: open-quote;
  font-size: 4em;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}

div.poem {
    white-space: pre-wrap;
  padding-left: 50px;
} 

</style>


</head>
<p>
<center>
<img src="EUFlagTear.jpg" alt="EUFlagTear.jpg" height="400"/>
</center>

<h1>Post Coronavirus Referendum to Rejoin EU</h1>
<h4>24-APR-2020</h4>
Watching the Coronavirus updates, looking at the death count, listening to the politicians answer every searching question about their handling of the crisis with "Our wonderful NHS". Thinking about how we clap at 8pm for all that wonderful NHS so often staffed with people from the EU. It occurred to me surely now really is the time sto stop and take stock before letting the Tories run us off a Brexit cliff edge?
<p>
So I prepared the text for a petition to post on the <a href="https://petition.parliament.uk/">Petition Parliament Website</a> 
<p>
<blockquote>
<h3>Request an Extension of the Transition Phase to Hold a Referendum to Rejoin</h3>
A 2% swing in the 2016 referendum would have seen Remain win. Since then, roughly 5% of the population has died to be replaced by a new 5% that has become eligible to vote. Request an extension to the Transition Phase for a Referendum with an option to Rejoin the EU.
<p>
Much has changed since 2016. We've learned how much the fortunes of the UK and EU are intertwined. 
<p>
Coronavirus has shown us how we depend on EU people that staff our hospitals, deliver our mail, bake our bread and contribute in so many ways.
<p>
When the pandemic is over, the global economy will be decimated. That's not the time to change our relationship with our main trading partner from cooperation to out and out competition.
<p>
Hard times are the very times when we most need to all pull together
</blockquote>
<p>
Before uploading it I sent it out to about twelve people. Eleven of them I know to be pro EU. I received three replies and all but one of them were particularly close to me so to sme extent have to to count has half replies. It was undoubtedly a lesson learned. A petition like this would only have the slightest hope of achieving something if seventeen million people voted for it. Even within my own bubble of pro EU supporters there wasn't really a glimmer of interest.
<p>
