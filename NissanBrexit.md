#Nissan Post Brexit

#### Sources

Article | Date | Source
-|-|-|
<a href="https://www.bbc.com/news/business-50000530">Nissan Europe 'unsustainable' in no-deal Brexit</a> | 10-OCT-2019 | BBC News
<a href="https://www.theguardian.com/business/2020/feb/03/nissan-eu-uk-hard-brexit">Nissan 'could pull out of EU and expand in UK after hard Brexit'</a> | 3-FEB-2020 | The guardian Online
<a href="https://www.ft.com/content/c4f0d1e2-4442-11ea-a43a-c4b328d9061c">Nissan drafts plan to double down on UK under hard Brexit</a> | 2-FEB-2020 | The FT
<a href="https://europe.autonews.com/automakers/nissan-warns-about-future-european-plants-brexit-weighs">Nissan warns about future of European plants as Brexit weighs</a> | 24-FEB-2020 | Automotive News Europe
<a href="https://www.spectator.co.uk/article/nissan-has-proven-again-that-the-hardcore-remainers-were-wrong">What happened to Brexit meaning the end of Nissan’s Sunderland plant?</a> | 28-MAY-2020 | The Spectator


